package zengine.interfaces;

/**
 * This interface represents a generic InputConstant that returns its specific
 * integer code.
 */
public interface ZengineInputConstant {

    /**
     * returns the int value associated with this constant.
     * 
     * @return the int value associated with this constant
     */
    int getValue();
}
